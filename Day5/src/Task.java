import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Task implements Comparable<Task> {
	public Scanner sc = new Scanner(System.in);
	private int TaskId;
	private String TaskTitle;
	private String TaskText;
	private String assignedTo;
	private String date;
	private Boolean status = false;

	public Task() {
	}

	public Task(int taskId, String taskTitle, String taskText, String assignedTo) {
		TaskId = taskId;
		TaskTitle = taskTitle;
		TaskText = taskText;
		this.assignedTo = assignedTo;
	}

	public Task(int taskId, String taskTitle, String taskText, String assignedTo, String date, Boolean status) {
		super();
		TaskId = taskId;
		TaskTitle = taskTitle;
		TaskText = taskText;
		this.assignedTo = assignedTo;
		this.date = date;
		this.status = status;
	}

	public int getTaskId() {
		return TaskId;
	}

	public void setTaskId(int taskId) {
		TaskId = taskId;
	}

	public String getTaskTitle() {
		return TaskTitle;
	}

	public void setTaskTitle(String taskTitle) {
		TaskTitle = taskTitle;
	}

	public String getTaskText() {
		return TaskText;
	}

	public void setTaskText(String taskText) {
		TaskText = taskText;
	}

	public String getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	@Override
	public int compareTo(Task o) {
		return 0;
	}

}