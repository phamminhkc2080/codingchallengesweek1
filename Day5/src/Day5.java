import java.util.ArrayList;
import java.util.Scanner;

public class Day5 {
	public static void main(String[] args) {
		ArrayList<User> arrUser = new ArrayList<>();
		User user = new User("user1", "password1", "Client");
		arrUser.add(user);
		User user1 = new User("user2", "password2", "Visitor");
		arrUser.add(user1);
		User user2 = new User("user3", "password3", "Client");
		arrUser.add(user2);

		Client client = new Client();
		Visitor visitor = new Visitor();
		Scanner sc = new Scanner(System.in);
		System.out.println("Welcom to my project! Please log in first!");
		boolean tt = true;
		do {
			System.out.print("Username: ");
			String userString = sc.nextLine();
			System.out.print("Password: ");
			String passString = sc.nextLine();
			for (int i = 0; i < arrUser.size(); i++) {
				if (userString.equals(arrUser.get(i).getUsername())
						&& passString.equals(arrUser.get(i).getPassword())) {
					if (arrUser.get(i).getPosition().equals("Client")) {
						client.menu();
					} else if (arrUser.get(i).getPosition().equals("Visitor")) {
						visitor.DisplaybyAssignTask(arrUser.get(i).getUsername());
					}
				} else {
					tt = false;
				}
			}
			if (tt = false) {
				System.out.println("I can't find your account. Please try again!");
			}
		} while (tt = false);

	}

}
