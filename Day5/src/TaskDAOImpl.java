import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Scanner;

public class TaskDAOImpl implements TaskDAO {

	public ArrayList<Task> arrTasks = new ArrayList<>();
	public Scanner sc = new Scanner(System.in);

	@Override
	public int searchTask() {
		System.out.println("Enter the Task you want to search: ");
		String search = sc.nextLine();
		int pos = -1;
		for (int i = 0; i < arrTasks.size(); i++) {
			if (arrTasks.get(i).getTaskText().toLowerCase().trim().equals(search.toLowerCase().trim())) {
				pos = i;
				break;
			}
		}
		return pos;

	}

	@Override
	public ArrayList<Task> getTaskbyAssign(String assign) {
		ArrayList<Task> arrTMP = new ArrayList<>();
		for (Task a : arrTasks) {
			if (a.getAssignedTo().toLowerCase().equals(assign)) {
				arrTMP.add(a);
			}
		}
		return arrTMP;
	}

	@Override
	public ArrayList<Task> getAllTask() {
		return arrTasks;
	}

	@Override
	public Task getTask(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addTask() {
		int id = arrTasks.size() + 1;
		System.out.println("Task " + (id) + ": ");
		System.out.print("Enter the Task Title: ");
		String taskTitle = sc.nextLine();
		System.out.print("Enter the Task Text: ");
		String taskText = sc.nextLine();
		System.out.print("Enter the assigned: ");
		String assignedTo = sc.nextLine();
		System.out.print("Enter the completion date: ");
		String date = sc.nextLine();
		Task tasks = new Task(id, taskTitle, taskText, assignedTo, date, false);
		arrTasks.add(tasks);
	}

	@Override
	public void updateTask() {
		int pos = searchTask();
		if (pos >= 0 && pos < arrTasks.size()) {
			System.out.print("Enter the Task Title: ");
			arrTasks.get(pos).setTaskTitle(sc.nextLine());
			System.out.print("Enter the Task Text: ");
			arrTasks.get(pos).setTaskText(sc.nextLine());
			System.out.print("Enter the assigned: ");
			arrTasks.get(pos).setAssignedTo(sc.nextLine());
			System.out.print("Enter the completion date: ");
			arrTasks.get(pos).setDate(sc.nextLine());
			System.out.print("Check completed: True / Fasle");
			arrTasks.get(pos).setStatus(Boolean.parseBoolean(sc.nextLine()));
			System.out.println("Update Successful");
		} else {
			System.out.println("No value exists");
		}
	}

	@Override
	public void deleteTask() {
		int pos = searchTask();
		if (pos >= 0) {
			Boolean dl;
			System.out.print("Are you sure you want to delete? True / False:  ");
			dl = Boolean.parseBoolean(sc.nextLine());
			if (dl) {
				arrTasks.remove(pos);
				System.out.println("Delete Successful");
			} else {
				System.out.println("Delete Fail");
			}

		} else {
			System.out.println("No value exists");
		}
	}

	@Override
	public void ResulftSearchTask(String search) {

		ArrayList<Task> arrTMP = new ArrayList<>();
		for (int i = 0; i < arrTasks.size(); i++) {
			if (arrTasks.get(i).getTaskText().toLowerCase().indexOf(search.toLowerCase()) == 0) {

				System.out.println("I can't find it");
			} else {
				arrTMP.add(arrTasks.get(i));
			}
		}
		for (Task task : arrTMP) {
			System.out.println("Task " + task.getTaskId() + " - title: " + task.getTaskTitle() + " - text: "
					+ task.getTaskText() + " - assigned to: " + task.getAssignedTo() + " - deadline: " + task.getDate()
					+ "- completed:  " + task.getStatus());
		}
	}

	@Override
	public void SortTask(ArrayList<Task> arr) {
		int c;
		boolean check = true;
		do {
			System.out.println("Sort by: 0. Decreasing / 1. Increasing");
			c = sc.nextInt();
			if (c >= 0 && c <= 1)
				check = false;
			else {
				System.out.println("Enter the value 0 or 1!");
			}
		} while (check);
		Sort(c, arr);
	}

	public void Sort(int i, ArrayList<Task> arr) {
		Collections.sort(arr, new Comparator<Task>() {

			@Override
			public int compare(Task t1, Task t2) {
				if (i == 0) {
					return (t1.getTaskTitle().compareTo(t2.getTaskTitle()));
				} else {
					return (t2.getTaskTitle().compareTo(t1.getTaskTitle()));
				}
			}
		});
	}

	@Override
	public void updateStatus(Task t) {
		System.out.print("Check completed: True / Fasle");
		t.setStatus(Boolean.parseBoolean(sc.nextLine()));
	}

}
