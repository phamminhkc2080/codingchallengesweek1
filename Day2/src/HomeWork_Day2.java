import java.util.Scanner;

public class HomeWork_Day2 {
	static Scanner ip = new Scanner(System.in);
	static String arr[];
	static int n;

	public void add() {
		System.out.print("Enter the number of arrays : ");
		n = ip.nextInt();
		arr = new String[n];
		System.out.println("Enter task to add ");
		for (int i = 0; i < arr.length; i++) {
			System.out.println("Task" + (i + 1) + " : ");
			arr[i] = ip.next();

		}
	}

	public void print() {
		for (int i = 0; i < n; i++) {
			System.out.println("Task" + (i + 1) + " : " + arr[i]);
		}
	}

	public void update() {
		System.out.println("Enter the task which you would like to update : ");
		String old = ip.next();
		System.out.println("Enter the new task which you want to add : ");
		String newS = ip.next();

		for (int i = 0; i < arr.length; i++) {
			if (arr[i].toLowerCase().equals(old.toLowerCase())) {
				arr[i] = newS;
				break;
			}
		}
	}

	public void find() {
		System.out.println("Enter a task which you suppose to search...");
		String search = ip.next();

		for (int i = 0; i < arr.length; i++) {
			if (arr[i].toLowerCase().equals(search.toLowerCase())) {
				System.out.println("The desired task is available at : " + arr[i]);
				break;
			}
		}
	}

	public void delete() {
		System.out.println("Enter a task you wanted to delete : ");
		String no = ip.next();
		for (int i = 0; i < n; i++) {
			if (arr[i].toLowerCase().equals(no.toLowerCase())) {
				System.out.println(arr[i] + " : pos  ");
				for (int j = i; j < n - 1; j++) {
					arr[j] = arr[j + 1];
				}
				n--;
				break;

			}
		}
	}

	public static void main(String[] args) {

		HomeWork_Day2 a = new HomeWork_Day2();
		a.add();
		int choice = 0;
		String s = "yes";
		do {

			System.out.println("1. Update a Task in Array");
			System.out.println("2. Find a Task from Array");
			System.out.println("3. Delete a Task from Array");
			System.out.println("0. Exit.");
			System.out.println("Enter your choice");
			choice = ip.nextInt();
			switch (choice) {
			case 1:
				a.update();
				break;
			case 2:
				a.find();
				break;
			case 3:
				a.delete();
				break;
			case 0:
				System.exit(1);

			}
			a.print();
			System.out.println("Would you like to continue?(yes/no)");
			s = ip.next();
		} while (s.equals("yes"));

	}

}
