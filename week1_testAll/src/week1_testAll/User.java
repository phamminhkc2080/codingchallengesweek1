package week1_testAll;

import java.util.ArrayList;

public class User {
	int userId;
	String userName, password, email;

	ArrayList<Integer> favourite;

	public User() {
		super();
	}

	public User(int userId, String userName, String password, String email, ArrayList<Integer> favourite) {
		super();
		this.userName = userName;
		this.password = password;
		this.email = email;
		this.userId = userId;
		this.favourite = favourite;
	}

	public String toStringCTHDN() {
		String t = favourite.get(0).toString();
		for (int i = 1; i < favourite.size(); i++) {
			t = t + " / " + favourite.get(i).toString();
		}
		return t;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public ArrayList<Integer> getFavourite() {
		return favourite;
	}

	public void setFavourite(ArrayList<Integer> favourite) {
		this.favourite = favourite;
	}

}
