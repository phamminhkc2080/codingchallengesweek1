package week1_testAll;

import java.util.ArrayList;
import java.util.Scanner;

public class MagicOfBooks {
	Scanner scanner = new Scanner(System.in);

	public void DisplayBook(ArrayList<Book> arrBook) {
		for (int i = 0; i < arrBook.size(); i++) {
			System.out.println(arrBook.get(i).toString());
		}
	}

	public void SearchbyID(ArrayList<Book> arrBook, int id) {
		for (int i = 0; i < arrBook.size(); i++) {
			if (arrBook.get(i).getBookId() == id) {
				System.out.println(arrBook.get(i).getBookId() + " - " + arrBook.get(i).getBookName());
			}
		}
	}

	public void DisplayBookbyID(ArrayList<Book> arrBook, int id) {
		for (Book book : arrBook) {
			if (book.getBookId() == id) {
				System.out.println(book.toString());
				break;
			}
		}
	}

}
