package week1_testAll;

import java.util.ArrayList;
import java.util.Scanner;

public class week1_main {
	public static void Login(ArrayList<User> user, ArrayList<Book> arrBook) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Please log in first!");
		boolean tt = true;
		do {
			System.out.print("Username: ");
			String userString = sc.nextLine();
			System.out.print("Password: ");
			String passString = sc.nextLine();
			for (int i = 0; i < user.size(); i++) {
				if (userString.equals(user.get(i).getUserName()) && passString.equals(user.get(i).getPassword())) {
					UserController userImpl = new UserController();
					userImpl.Menu(arrBook, user, userString);
				} else {
					tt = false;
				}
			}
			if (tt == false) {
				System.out.println("404");
			}
		} while (tt == false);
	}

	public static void main(String[] args) {
		ArrayList<Book> arrBookAttributes = new ArrayList<>();
		arrBookAttributes.add(new Book(1, "Ty quay", "Chung chung", "nice book"));
		arrBookAttributes.add(new Book(2, "Thai Giam", "Tu Tu", "this is a very nice book"));
		arrBookAttributes.add(new Book(3, "Huynh Huynh", "Dau Dau", "i think this is the best"));

		ArrayList<User> arruserAtributes = new ArrayList<>();
		ArrayList<Integer> Favorite = new ArrayList<>();

		Favorite.add(1);
		Favorite.add(2);
		arruserAtributes.add(new User(1, "user1", "minh", "minh@hcl.com", Favorite));

		ArrayList<Integer> Favorite2 = new ArrayList<>();
		Favorite2.add(1);
		Favorite2.add(3);
		arruserAtributes.add(new User(1, "user2", "minh", "minh1@hcl.com", Favorite2));

		ArrayList<Integer> Favorite3 = new ArrayList<>();
		Favorite3.add(2);
		Favorite3.add(3);
		arruserAtributes.add(new User(1, "user3", "minh", "minh2@hcl.com", Favorite2));

		Login(arruserAtributes, arrBookAttributes);

	}

}
