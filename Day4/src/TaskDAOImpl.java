import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class TaskDAOImpl implements TaskDAO {

	public ArrayList<Task> arrTasks = new ArrayList<>();
	public Scanner sc = new Scanner(System.in);

	@Override
	public int searchTask() {
		System.out.println("Enter the Task you want to search: ");
		String search = sc.nextLine();
		int pos = -1;
		for (int i = 0; i < arrTasks.size(); i++) {
			if (arrTasks.get(i).getTaskText().toLowerCase().trim().equals(search.toLowerCase().trim())) {
				pos = i;
				break;
			}
		}
		return pos;

	}

	@Override
	public ArrayList<Task> getTaskbyAssign(String assign) {
		ArrayList<Task> arrTMP = new ArrayList<>();
		for (Task a : arrTasks) {
			if (a.getAssignedTo().toLowerCase().equals(assign)) {
				arrTMP.add(a);
			}
		}
		return arrTMP;
	}

	@Override
	public ArrayList<Task> getAllTask() {
		return arrTasks;
	}

	@Override
	public Task getTask(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addTask() {
		int id = arrTasks.size() + 1;
		System.out.println("Task " + (id) + ": ");
		System.out.print("Enter the Task Title: ");
		String taskTitle = sc.nextLine();
		System.out.print("Enter the Task Text: ");
		String taskText = sc.nextLine();
		System.out.print("Enter the assigned: ");
		String assignedTo = sc.nextLine();
		Task tasks = new Task(id, taskTitle, taskText, assignedTo);
		arrTasks.add(tasks);
	}

	@Override
	public void updateTask() {
		int pos = searchTask();
		if (pos >= 0 && pos < arrTasks.size()) {
			System.out.print("Enter the Task Title: ");
			arrTasks.get(pos).setTaskTitle(sc.nextLine());
			System.out.print("Enter the Task Text: ");
			arrTasks.get(pos).setTaskText(sc.nextLine());
			System.out.print("Enter the assigned: ");
			arrTasks.get(pos).setAssignedTo(sc.nextLine());
			System.out.println("Update Successful");
		} else {
			System.out.println("No value exists");
		}
	}

	@Override
	public void deleteTask() {
		int pos = searchTask();
		if (pos >= 0) {
			Boolean dl;
			System.out.print("Are you sure you want to delete? True / False:  ");
			dl = Boolean.parseBoolean(sc.nextLine());
			if (dl) {
				arrTasks.remove(pos);
				System.out.println("Delete Successful");
			} else {
				System.out.println("Delete Fail");
			}

		} else {
			System.out.println("No value exists");
		}
	}

	@Override
	public void ResulftSearchTask(String search) {

		ArrayList<Task> arrTMP = new ArrayList<>();
		for (int i = 0; i < arrTasks.size(); i++) {
			if (arrTasks.get(i).getTaskText().toLowerCase().indexOf(search.toLowerCase()) == 0) {
				System.out.println("I can't find it");
			} else {
				arrTMP.add(arrTasks.get(i));
			}
		}
		for (Task task : arrTMP) {
			System.out.println("Task " + task.getTaskId() + " - title: " + task.getTaskTitle() + " - text: "
					+ task.getTaskText() + " - assigned to: " + task.getAssignedTo());
		}
	}

}
