import java.util.ArrayList;

public interface TaskDAO {

	public ArrayList<Task> getAllTask();

	public ArrayList<Task> getTaskbyAssign(String assign);

	public Task getTask(int id);

	public void addTask();

	public void updateTask();

	public void deleteTask();

	public int searchTask();

	public void ResulftSearchTask(String search);

}
