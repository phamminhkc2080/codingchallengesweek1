import java.util.Arrays;
import java.util.Scanner;

public class Day1_Ex {

	public static void main(String[] args) {

		Scanner ip = new Scanner(System.in);
		// Ex1
		String name = "";
		System.out.print("Import your name : ");
		name = ip.nextLine();
		System.out.println("My name is : " + name);
		System.out.println("===================================================");
		// Ex2
		String[] task = { "Playing", "Sleeping", "Sleeping", "Sleeping", "Have lunch" };

		for (int i = 0; i < task.length; i++) {
			System.out.println("Task " + i + " : " + task[i]);
		}

		System.out.println("===================================================");
		for (int i = task.length - 1; i >= 0; i--) {
			System.out.println("Task " + i + " : " + task[i]);
		}

		System.out.println("===================================================");
		Arrays.sort(task);
		for (int i = 0; i < task.length; i++) {
			System.out.println("Task " + i + " : " + task[i]);
		}

		System.out.println("===========================");
		//
		for (int i = 0; i < task.length; i++) {
			boolean check = true;
			int count = 1;
			if (i == 0)
				check = true;
			else {
				for (int k = 0; k < i; k++) {
					if (task[i].toLowerCase().equals(task[k].toLowerCase())) {
						check = false;
						break;
					}
				}
			}
			if (check == true) {
				for (int j = i + 1; j < task.length; j++) {
					if (task[i].toLowerCase().equals(task[j].toLowerCase())) {
						count++;
					}
				}
				System.out.println(task[i] + " " + count + " times");
			}
		}

	}

}
